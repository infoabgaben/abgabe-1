/**
 *  @authors Laurids Bornhoeft, Lennart Ferlings, Thi Thu Hien Nguyen
 */
package info.insertionsort;
//////////////////////////

import java.util.Random;

//Informatik II - SoSe 2020
//Task 1.2. a) b) c)
//////////////////////////

//////////////////////////
//Submission by:
//
//	Laurids Bornhoeft, Lennart Ferlings, Thi Thu Hien Nguyen
//
//////////////////////////

public class InsertionSort {

//////////////////////
// Main function
	public static void main(String[] args) {
// task 1.2.a)
		int[] list = { 2, 1, 6, 2, 4, 1, 6 };
//printList(list);
		int[] newlist = insertionSort(list);
		printList(newlist);		
		
// task 1.2b)c)
		analyzeTimeComplexity();
	}

//////////////////////
// Helper function to print the elements of a list
	private static void printList(int[] list) {
		for (int i = 0; i < list.length; i++) {
			System.out.println(list[i]);
		}
	}

//////////////////////
// Task 1.2.a)
// Implement the insertion sort algorithm based on
// the lecture 1 (see slides 40 - 44)
	public static int[] insertionSort(int[] input_list) {
		for(int j = 1; j < input_list.length;j++) { //iterate through input array
			int tmp = input_list[j]; //save current item
			int i = j - 1; //end of presorted part
			
			while(i >= 0 && input_list[i] > tmp) { //iterate through presorted part
				input_list[i + 1] = input_list[i]; //move items
				i--;
			}
			input_list[i + 1] = tmp; //insert item
		}
		return input_list; //return sorted array
	}

//////////////////////
// Task 1.2.b)
// Create three lists of size n that create lists that are
// average, best and worst case
//
// You can create random numbers by first creating a new
// random number generator:
//   Random rng = new Random();
// Then retrieving a random number between 0 and K-1 using:
//   int value = rng.nextInt(K);
	private static int[] generateAverageCase(int n) {
		int[] result = new int[n];
		Random rdm = new Random();
		
		for(int i = 0; i < n; i++) //generate random number for every slot
			result[i] = rdm.nextInt(n);
		
		return result;
	}

	private static int[] generateWorstCase(int n) {
		int[] result = new int[n];
		int j = n - 1;
		
		for(int i = 0; i < n; i++)  //iterate
			result[i] = j--; //put in the "reversed" index
		return result;
	}

	private static int[] generateBestCase(int n) {
		int[] result = new int[n];
		
		for(int i = 0; i < n; i++)  //iterate
			result[i] = i; //put in the index
		return result;
	}
	
//////////////////////
// Task 1.2.c)
// Apply the function from 1.2.b for ascending n. Experiment
// which nMin, nMax and step makes sense when iterating over n
//   for(int n = nMin; n <= nMax; n += step)
// and measure the time of the Insertion Sort function call.
// Plot the results in an application of your choice
//
// You can get the current time using this java function:
//    long start = System.currentTimeMillis();
	private static void analyzeTimeComplexity() {
		int nMin = 1000, nMax = 101000, step = 10000; //initalizing parameters for test
		
		System.out.println("Testing time complexity with the following parameters: ");
		System.out.println("nMin = " + nMin);
		System.out.println("nMax = " + nMax);
		System.out.println("step = " + step);
		
		System.out.println();
		System.out.println("Testing worst case...");
		for(int n = nMin; n <= nMax; n += step) {
			int[] worst = generateWorstCase(n); //gen list
			long start = System.currentTimeMillis(); //start timer
			insertionSort(worst); //run sort
			long time = System.currentTimeMillis() - start; //stop timer
			System.out.println("n = " + n + "\t-->\t" + time + "ms"); //print result
		}

		System.out.println();
		System.out.println("Testing average case...");
		for(int n = nMin; n <= nMax; n += step) {
			int[] average = generateAverageCase(n);//gen list
			long start = System.currentTimeMillis(); //start timer
			insertionSort(average); //run sort
			long time = System.currentTimeMillis() - start; //stop timer
			System.out.println("n = " + n + "\t-->\t" + time + "ms"); //print result
		}
		
		System.out.println();
		System.out.println("Testing best case...");
		for(int n = nMin; n <= nMax; n += step) {
			int[] best = generateBestCase(n);//gen list
			long start = System.currentTimeMillis(); //start timer
			insertionSort(best); //run sort
			long time = System.currentTimeMillis() - start; //stop timer
			System.out.println("n = " + n + "\t-->\t" + time + "ms"); //print result
		}
	}
}
